#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include "argparser.hpp"
#include "window.hpp"

#define PERIOD_COUNTER 200
#define TARGET_NUMBER 2048

class Game2048 {
public:

    // initialization of the game
    explicit Game2048(size_t size) :
            quit(false),
            win(std::make_unique<Window>(std::cout, size)),
            size(size),
            board(size, std::vector<int>(size, 0)) {
        spawnNumber();
        spawnNumber();
        win->redraw(board, false, false);
    }

    ~Game2048(){control();}

    // getters and setters for private values

    std::unique_ptr<Window>& getWin() {
        return win;
    }

    std::vector<std::vector<int>>& getBoard() {
    return board;
    }

    // control function - checks whether the game was won by getting 2048 points,
    // chekcs whether the board is full

    void control(){
        bool is_win = false;

        bool is_draw = true;
        // bool boardChanged = false;

        for (size_t i = 0; i < size; ++i) {
            for (size_t j = 0; j < size; ++j) {
                if (board[i][j] == TARGET_NUMBER) {
                    is_win = true;
                    is_draw = false;
                    break;
                }
                if (board[i][j] == 0) {
                    is_draw = false;
                }
            }
        }

        if (is_win || is_draw) {
            setQuit(true);
            win->redraw(board, is_win, is_draw);
        }
        std::cout << std::endl;
    }

    // collects input from user and calls game controlling functions
    void input(){
        std::unique_lock<std::mutex> lg(mutex);
        lg.unlock();
        if (quit)
            return;

        char c;
        std::cin >> c;
        if (c == 'q') {
            setQuit(true);
        } else if (c == 'w' || c == 'a' || c == 's' || c == 'd') {
            prevBoard = board;
            bool boardChanged = false;
            moveBoard(c);
            for (size_t i = 0; i < size; ++i) {
                for (size_t j = 0; j < size; ++j) {
                    if (board[i][j] != prevBoard[i][j]) {
                        boardChanged = true;
                        break;
                    }
                }
                if (boardChanged) {
                    spawnNumber();
                    break;
                }
            }
            control();
            win->redraw(board, false, false);
        }
    }

    bool getQuit() const {
        return quit;
    }
    // if user puts in q, quit is true and all threads are joined and game ends
    void setQuit(const bool qq) {
        quit = qq;
    }

private:
    bool quit;
    std::unique_ptr<Window> win;
    size_t size;
    std::vector<std::vector<int>> board;
    std::mutex mutex;
    std::vector<std::vector<int>> prevBoard;

    // decides how to change the game state and proceeds moving
    void moveBoard(char direction) {
        switch (direction) {
            case 'w':
                moveUp();
                break;
            case 'a':
                moveLeft();
                break;
            case 's':
                moveDown();
                break;
            case 'd':
                moveRight();
                break;
            default:
                break;
        }
    }

    // runs when user puts in w

    void moveUp(){
        for (size_t j = 0; j < size; ++j) {
            size_t currIdx = 0;

            for (size_t i = 1; i < size; ++i) {
                if (board[i][j] != 0) {
                    if (board[currIdx][j] == 0) {
                        board[currIdx][j] = board[i][j];
                        board[i][j] = 0;
                    } else if (board[currIdx][j] == board[i][j]) {
                        board[currIdx][j] *= 2;
                        board[i][j] = 0;
                        ++currIdx;
                    } else {
                        ++currIdx;
                        if (currIdx != i) {
                            board[currIdx][j] = board[i][j];
                            board[i][j] = 0;
                        }
                    }
                }
            }
        }
    }

    // runs when user puts in d

    void moveLeft(){
        for (size_t i = 0; i < size; ++i) {
            size_t currIdx = 0;

            for (size_t j = 1; j < size; ++j) {
                if (board[i][j] != 0) {
                    if (board[i][currIdx] == 0) {
                        board[i][currIdx] = board[i][j];
                        board[i][j] = 0;
                    } else if (board[i][currIdx] == board[i][j]) {
                        board[i][currIdx] *= 2;
                        board[i][j] = 0;
                        ++currIdx;
                    } else {
                        ++currIdx;
                        if (currIdx != j) {
                            board[i][currIdx] = board[i][j];
                            board[i][j] = 0;
                        }
                    }
                }
            }
        }
    }


    // runs when user puts in s

    void moveDown(){
        for (size_t j = 0; j < size; ++j) {
            size_t currIdx = size - 1;

            for (size_t i = size - 2; i < size; --i) {
                if (board[i][j] != 0) {
                    if (board[currIdx][j] == 0) {
                        board[currIdx][j] = board[i][j];
                        board[i][j] = 0;
                    } else if (board[currIdx][j] == board[i][j]) {
                        board[currIdx][j] *= 2;
                        board[i][j] = 0;
                        --currIdx;
                    } else {
                        --currIdx;
                        if (currIdx != i) {
                            board[currIdx][j] = board[i][j];
                            board[i][j] = 0;
                        }
                    }
                }
            }
        }
    }

    // runs when user puts in a

    void moveRight() {
        for (size_t i = 0; i < size; ++i) {
            size_t currIdx = size - 1;

            for (size_t j = size - 2; j < size; --j) {
                if (board[i][j] != 0) {
                    if (board[i][currIdx] == 0) {
                        board[i][currIdx] = board[i][j];
                        board[i][j] = 0;
                    } else if (board[i][currIdx] == board[i][j]) {
                        board[i][currIdx] *= 2;
                        board[i][j] = 0;
                        --currIdx;
                    } else {
                        --currIdx;
                        if (currIdx != j) {
                            board[i][currIdx] = board[i][j];
                            board[i][j] = 0;
                        }
                    }
                }
            }
        }
    }

    // runs after any move if game state changed

    void spawnNumber() {
        std::vector<std::pair<size_t, size_t>> emptyCells;
        for (size_t i = 0; i < size; ++i) {
            for (size_t j = 0; j < size; ++j) {
                if (board[i][j] == 0) {
                    emptyCells.emplace_back(i, j);
                }
            }
        }

        if (!emptyCells.empty()) {
            // picks the random tile and fill the random number in

            size_t index = rand()% emptyCells.size();

            size_t value = (rand()%2+1) * 2;
            board[emptyCells[index].first][emptyCells[index].second]= value;
        }
    }
};

// main function runs the program and inits the threads

int main(int argc, char *argv[]) {
    ProgramArguments programArgs = ArgParser::parseProgramArguments(argc, argv);
    if (programArgs.size < 4 || programArgs.size > 10) {
        std::cerr << "Size must be between 4 and 10." << std::endl;
        return 1;
    }

    set_raw(true);
    Game2048 game(programArgs.size);
    auto inputThread = [&game]() {
        bool q = false;
        while (!q) {
            game.input();
            q = game.getQuit();
        }
    };
    auto outputThread = [&game]() {
        bool q = false;
        while (!q) {
            game.control();
            std::this_thread::sleep_for(std::chrono::milliseconds(PERIOD_COUNTER));
            game.getWin()->redraw(game.getBoard(), false, false);
            q = game.getQuit();
        }
    };
    std::thread t1(inputThread);
    std::thread t2(outputThread);
    t1.join();
    t2.join();
    set_raw(false);
    return 0;
}