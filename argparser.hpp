#ifndef GAME_2048_ARGPARSER_H
#define GAME_2048_ARGPARSER_H

#include <iostream>
#include <string>
#include <cstring>

#define ARG_SIZE_DEFAULT 4
#define ARG_SIZE_MIN 4
#define ARG_SIZE_MAX 10

// sets raw mode
void set_raw(bool set);
// program settings
struct ProgramArguments {
    int size;
};

class ArgParser {
public:
    // sets the program
    static ProgramArguments parseProgramArguments(int argc, char *argv[]);
private:
    static int str2int(int *res, const char *str);
};

#endif //GAME_2048_ARGPARSER_H
