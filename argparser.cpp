#include "argparser.hpp"

// collects user input and argument while running the game, look for -s, -size, --help
// handles the program end when user puts in incorrect agrument or --help, otherwise proceeds

ProgramArguments ArgParser::parseProgramArguments(int argc, char *argv[]) {
    auto parsedProgramArguments = ProgramArguments{ARG_SIZE_DEFAULT};
    for (int i = 1; i < argc; ++i) {
        if (std::strcmp(argv[i], "--help") == 0) {
            std::cout << "Usage: ./2024 [-s|-size <size>] [--help]\nOptions:\n-s, -size <size>   Set the size of the game board (4-10)\n--help              Display this help message\n";
            std::cout << "\n To play the game use keys W,A,S,D\n";
            std::exit(0);
        }
        int tmp;
        if (i + 1 < argc && std::strlen(argv[i]) > 1) {
            switch (argv[i][1]) {
                case 's':
                    if ((std::strlen(argv[i]) <= 2 || !std::strcmp(argv[i], "-size")) &&
                        str2int(&tmp, argv[i + 1]) >= 0 && tmp >= ARG_SIZE_MIN && tmp <= ARG_SIZE_MAX) {
                        parsedProgramArguments.size = tmp;
                        i++; // skip the next argument as it has been processed
                    } else {
                        std::cerr << "Error: Invalid argument for '-s' or '--size' to set the size of the board. Use '--help' for usage information.\n";
                        std::exit(1);
                    }
                    break;
                default:
                    std::cerr << "Error: Unrecognized option '" << argv[i] << "'. Use '--help' for usage information.\n";
                    std::exit(1);
            }
        } else {
            std::cerr << "Error: Incomplete argument '" << argv[i] << "'. Use '--help' for usage information.\n";
            std::exit(1);
        }
    }

    return parsedProgramArguments;
}

// parses int

int ArgParser::str2int(int *res, const char *str) {
    try {
        *res = std::stoi(str);
    } catch (const std::invalid_argument &e) {
        return -1;
    } catch (const std::out_of_range &e) {
        return -2;
    }
    return 0;
}

void set_raw(bool set) {
    if (set) {
        system("stty raw");
    } else {
        system("stty -raw");
    }
}