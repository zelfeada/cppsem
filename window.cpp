#include "window.hpp"
#include <iostream>

Window::Window(std::ostream &outputStream, size_t size) : outputStream(outputStream), size(size) {}

void Window::redraw(std::vector<std::vector<int>> &board, bool is_win, bool is_draw) {
    renderWindow(board);
    redrawResult(is_win, is_draw);
}

// draws if the game ended

void Window::redrawResult(bool is_win, bool is_draw) {
    if (is_win) {
        outputStream << COLOR_WIN << "\n\r   !!! YOU WIN !!!   \n" << ANSI_COLOR_RESET;
    } else if (is_draw) {
        outputStream << COLOR_DRAW << "\n\r   !!! ZOU LOSE !!!   \n" << ANSI_COLOR_RESET;
    }
}

// draws the tile

void tile(std::ostream& os, int value, size_t width) {
    // get number of digits
    size_t numDigits = (value == 0) ? 1 : 0;
    int temp = value;
    while (temp != 0) {
        temp /= 10;
        ++numDigits;
    }

    size_t padding = width - numDigits;

    // draws if cell is not empty
    if (value) {
        for (size_t i = 0; i < padding; ++i) {
                os << ' ';
            }
    }
    // draws if cell is empty
    else {
        for (size_t i = 0; i < padding+1; ++i) {
                os << ' ';
            }
    }

    if (value != 0) {
        os << COLOR_DEFAULT << value << ANSI_COLOR_RESET;
    }
}

void Window::renderWindow(std::vector<std::vector<int>>& board) {
    std::stringstream buffer;
    buffer << ANSI_CLEAR << ANSI_COLOR_RESET;
    buffer << "\r";

    // gets the biggest number
    size_t maxDigits = 1;
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            int number = board[i][j];
            size_t digits = (number == 0) ? 1 : 0;
            int temp = number;
            while (temp != 0) {
                temp /= 10;
                ++digits;
            }
            maxDigits = std::max(maxDigits, digits);
        }
    }

    // calculates the col width based on the biggest number
    size_t columnWidth = maxDigits + 2;

    for (size_t i = 0; i < size; ++i) {
        buffer << "\r";
        for (size_t j = 0; j < size; ++j) {
            buffer << "| ";
            tile(buffer, board[i][j], columnWidth);
            buffer << ' ';
        }

        buffer << "|";
        buffer << "\n\r";
        for (size_t j = 0; j < (columnWidth + 3) * size + 1; ++j)
            buffer << "-";
        buffer << std::endl;
    }
    buffer << "\r";
    outputStream << buffer.str() << std::endl;
}