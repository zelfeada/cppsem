#ifndef GAME_2048_WINDOW_H
#define GAME_2048_WINDOW_H

#include <sstream>
#include <vector>

#define ANSI_CLEAR "\x1B[2J\x1B[H"  // Clear terminal
#define ANSI_COLOR_RESET "\x1B[m"   // Reset colors
#define COLOR_DEFAULT "\x1B[97m"     // Default color for numbers
#define COLOR_WIN "\x1B[48;5;52m\x1B[38;5;208m"   // Orange color on brown background for win message
#define COLOR_DRAW "\x1B[48;5;17m\x1B[38;5;75m"    // Light blue color on blue background for draw message

class Window {
public:
    explicit Window(std::ostream &outputStream, size_t size);
    void redraw(std::vector<std::vector<int>> &board, bool is_win, bool is_draw);
    void renderWindow(std::vector<std::vector<int>> &board);
    void redrawResult(bool is_win, bool is_draw);
private:
    std::ostream &outputStream;
    size_t size;
};

#endif //GAME_2048_WINDOW_H